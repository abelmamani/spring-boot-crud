package ar.edu.undec.apringbootcrud.repository;

import ar.edu.undec.apringbootcrud.crud.ProductCRUD;
import ar.edu.undec.apringbootcrud.entity.ProductEntity;
import ar.edu.undec.apringbootcrud.exception.ProductException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductRepoImplementation implements ProductRepository{
    private ProductCRUD productCRUD;

    @Autowired
    public ProductRepoImplementation(ProductCRUD productCRUD) {
        this.productCRUD = productCRUD;
    }

    @Override
    public Collection<ProductEntity> getProducts() {
        return productCRUD.findAll().stream().collect(Collectors.toList());
    }

    @Override
    public ProductEntity createProdudct(ProductEntity productEntity) {
        ProductEntity product = ProductEntity.getInstance(null,
                productEntity.getName(),
                productEntity.getDescription(),
                productEntity.getStock(),
                productEntity.getPrice(),
                productEntity.getCreateAt());
        return productCRUD.save(productEntity);
    }

    @Override
    public ProductEntity getProduct(Integer id) {
        Optional<ProductEntity> product = productCRUD.findById(id);
        if(product.isEmpty())
            throw new ProductException("the product with id "+id+" not exist");
        return product.get();
    }

    @Override
    public ProductEntity updateProdudct(ProductEntity productEntity) {
        if(!productCRUD.existsById(productEntity.getId()))
            throw new ProductException("the product with id "+productEntity.getId()+"not exist");
        ProductEntity product = ProductEntity.getInstance(productEntity.getId(),
                productEntity.getName(),
                productEntity.getDescription(),
                productEntity.getStock(),
                productEntity.getPrice(),
                productEntity.getCreateAt());
        return productCRUD.save(productEntity);
    }

    @Override
    public void deleteProduct(Integer id) {
        if(!productCRUD.existsById(id))
            throw new ProductException("the product with id "+id+"not exist");
        productCRUD.deleteById(id);
    }
}
