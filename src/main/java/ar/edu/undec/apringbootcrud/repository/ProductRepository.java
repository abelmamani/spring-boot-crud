package ar.edu.undec.apringbootcrud.repository;

import ar.edu.undec.apringbootcrud.entity.ProductEntity;

import java.util.Collection;

public interface ProductRepository {

    Collection<ProductEntity> getProducts();
    ProductEntity createProdudct(ProductEntity productEntity);
    ProductEntity getProduct(Integer id);
    ProductEntity updateProdudct(ProductEntity productEntity);
    void deleteProduct(Integer id);
}
