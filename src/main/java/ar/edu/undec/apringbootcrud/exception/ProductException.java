package ar.edu.undec.apringbootcrud.exception;

public class ProductException extends RuntimeException{
    public ProductException(String msg){
        super(msg);
    }
}
