package ar.edu.undec.apringbootcrud.entity;

import ar.edu.undec.apringbootcrud.exception.ProductException;
import jakarta.persistence.*;

import java.time.LocalDate;

@Entity
@Table(name = "products")
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String description;
    private Integer stock;
    private Double price;
    private LocalDate createAt;

    public ProductEntity(){}
    public ProductEntity(Integer id, String name, String description, Integer stock, Double price, LocalDate createAt) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.stock = stock;
        this.price = price;
        this.createAt = createAt;
    }
    public static ProductEntity getInstance(Integer id, String name, String description, Integer stock, Double price, LocalDate createAt) {
        if(name == null)
            throw new ProductException("the name cannot be null");
        if(name.isBlank())
            throw new ProductException("the name cannot be empty");
        if(description == null)
            throw new ProductException("the description cannot be null");
        if(description.isBlank())
            throw new ProductException("the description cannot be empty");
        if(stock == null)
            throw new ProductException("the stock cannot be null");
        if(stock < 0)
            throw new ProductException("the stock cannot be negative");
        if(price == null)
            throw new ProductException("the price cannot be null");
        if(price < 100)
            throw new ProductException("the price must be more 99");
        if(createAt == null)
            throw new ProductException("the create at cannot be null");
        return new ProductEntity(id, name, description, stock, price, createAt);
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Integer getStock() {
        return stock;
    }

    public Double getPrice() {
        return price;
    }

    public LocalDate getCreateAt() {
        return createAt;
    }
}
