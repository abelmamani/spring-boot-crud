package ar.edu.undec.apringbootcrud.crud;

import ar.edu.undec.apringbootcrud.entity.ProductEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
@Repository
public interface ProductCRUD extends CrudRepository<ProductEntity, Integer> {
    Collection<ProductEntity> findAll();
}
