package ar.edu.undec.apringbootcrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApringBootCrudApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApringBootCrudApplication.class, args);
    }

}
