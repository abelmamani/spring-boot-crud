package ar.edu.undec.apringbootcrud.utilities;

import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class ResponseManager {
    public static ResponseEntity<?> responseError(String msg){
        Map<String, String> response = new HashMap<>();
        response.put("message", msg);
        return ResponseEntity.badRequest().body(response);
    }
    public static ResponseEntity<?> responseSuccess(String msg){
        Map<String, String> response = new HashMap<>();
        response.put("message", msg);
        return ResponseEntity.ok(response);
    }
}
