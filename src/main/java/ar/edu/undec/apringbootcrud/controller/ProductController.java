package ar.edu.undec.apringbootcrud.controller;

import ar.edu.undec.apringbootcrud.entity.ProductEntity;
import ar.edu.undec.apringbootcrud.repository.ProductRepoImplementation;
import ar.edu.undec.apringbootcrud.utilities.ResponseManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("products")
@CrossOrigin("*")
public class ProductController {
    private ProductRepoImplementation productRepoImplementation;
    @Autowired
    public ProductController(ProductRepoImplementation productRepoImplementation) {
        this.productRepoImplementation = productRepoImplementation;
    }
    @GetMapping
    public ResponseEntity<?> getProducts(){
        try {
            return ResponseEntity.ok(productRepoImplementation.getProducts());
        }catch (RuntimeException exception){
            return ResponseManager.responseError(exception.getMessage());
        }
    }

    @PostMapping
    public ResponseEntity<?> createProduct(@RequestBody ProductEntity product){
        try {
            return ResponseEntity.created(null).body(productRepoImplementation.createProdudct(product));
        }catch (RuntimeException exception){
            return ResponseManager.responseError(exception.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getProduct(@PathVariable("id") Integer id){
        try {
            return ResponseEntity.ok(productRepoImplementation.getProduct(id));
        }catch (RuntimeException exception){
            return ResponseManager.responseError(exception.getMessage());
        }
    }

    @PutMapping
    public ResponseEntity<?> updateProduct(@RequestBody ProductEntity product){
        try {
            return ResponseEntity.ok(productRepoImplementation.updateProdudct(product));
        }catch (RuntimeException exception){
            return ResponseManager.responseError(exception.getMessage());
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable("id") Integer id){
        try {
            productRepoImplementation.deleteProduct(id);
            return ResponseManager.responseSuccess("proudct with id "+id+" deleted");
        }catch (RuntimeException exception){
            return ResponseManager.responseError(exception.getMessage());
        }
    }
}
